#ifndef ENEMY_H
#define ENEMY_H

#include <stdlib.h>
#include <time.h>
#include "./logic.h"
#include "./sprites.h"

/* Has offensive patterns. Returns 1 if it finds a pattern. Return 0 if nothing is found */
int attack_patterns()
{
   if (check_win() != 1)
   {
      if (player_is_circle == true)
      {
         /* Horizontal checks */
         if ((occup_pos.two == X && occup_pos.three == X) && occup_pos.one == empty) {
            check_free_pos();
            draw_X(posOne);
            return 1;
         }

         else if ((occup_pos.one == X && occup_pos.three == X) && occup_pos.two == empty) {
            check_free_pos();
            draw_X(posTwo);
            return 1;
         }

         else if ((occup_pos.one == X && occup_pos.two == X) && occup_pos.three == empty) {
            check_free_pos();
            draw_X(posThree);
            return 1;
         }

         else if ((occup_pos.five == X && occup_pos.six == X) && occup_pos.four == empty) {
            check_free_pos();
            draw_X(posFour);
            return 1;
         }
         
         else if ((occup_pos.four == X && occup_pos.six == X) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }

         else if ((occup_pos.four == X && occup_pos.five == X) && occup_pos.six == empty) {
            check_free_pos();
            draw_X(posSix);
            return 1;
         }

         else if ((occup_pos.eight == X && occup_pos.nine == X) && occup_pos.seven == empty) {
            check_free_pos();
            draw_X(posSeven);
            return 1;
         }
         
         else if ((occup_pos.seven == X && occup_pos.nine == X) && occup_pos.eight == empty) {
            check_free_pos();
            draw_X(posEight);
            return 1;
         }

         else if ((occup_pos.seven == X && occup_pos.eight == X) && occup_pos.nine == empty) {
            check_free_pos();
            draw_X(posNine);
            return 1;
         }

         /* Vertical checks */
         else if ((occup_pos.four == X && occup_pos.seven == X) && occup_pos.one == empty) {
            check_free_pos();
            draw_X(posOne);
            return 1;
         }
         
         else if ((occup_pos.five == X && occup_pos.eight == X) && occup_pos.two == empty) {
            check_free_pos();
            draw_X(posTwo);
            return 1;
         }

         else if ((occup_pos.six == X && occup_pos.nine == X) && occup_pos.three == empty) {
            check_free_pos();
            draw_X(posThree);
            return 1;
         }

         else if ((occup_pos.one == X && occup_pos.seven == X) && occup_pos.four == empty) {
            check_free_pos();
            draw_X(posFour);
            return 1;
         }

         else if ((occup_pos.two == X && occup_pos.eight == X) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }
         
         else if ((occup_pos.three == X && occup_pos.nine == X) && occup_pos.six == empty) {
            check_free_pos();
            draw_X(posSix);
            return 1;
         }

         else if ((occup_pos.one == X && occup_pos.four == X) && occup_pos.seven == empty) {
            check_free_pos();
            draw_X(posSeven);
            return 1;
         }

         else if ((occup_pos.two == X && occup_pos.five == X) && occup_pos.eight == empty) {
            check_free_pos();
            draw_X(posEight);
            return 1;
         }

         else if ((occup_pos.three == X && occup_pos.six == X) && occup_pos.nine == empty) {
            check_free_pos();
            draw_X(posNine);
            return 1;
         }

         /* Diagonal checks */
         else if ((occup_pos.five == X && occup_pos.nine == X) && occup_pos.one == empty) {
            check_free_pos();
            draw_X(posOne);
            return 1;
         }

         else if ((occup_pos.five == X && occup_pos.seven == X) && occup_pos.three == empty) {
            check_free_pos();
            draw_X(posThree);
            return 1;
         }

         else if ((occup_pos.five == X && occup_pos.three == X) && occup_pos.seven == empty) {
            check_free_pos();
            draw_X(posSeven);
            return 1;
         }

         else if ((occup_pos.five == X && occup_pos.one == X) && occup_pos.nine == empty) {
            check_free_pos();
            draw_X(posNine);
            return 1;
         }

         else if ((occup_pos.one == X && occup_pos.nine == X) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }

         else if ((occup_pos.three == X && occup_pos.seven == X) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }
      }
   }

   return 0;
}

/* Has defensive patterns. Returns 1 if it finds a pattern. Return 0 if nothing is found */
int defense_patterns()
{
   if (check_win() != 1)
   {
      if (player_is_circle == true)
      {
         /* Horizontal checks */
         if ((occup_pos.two == circle && occup_pos.three == circle) && occup_pos.one == empty) {
            check_free_pos();
            draw_X(posOne);
            return 1;
         }

         else if ((occup_pos.one == circle && occup_pos.three == circle) && occup_pos.two == empty) {
            check_free_pos();
            draw_X(posTwo);
            return 1;
         }

         else if ((occup_pos.one == circle && occup_pos.two == circle) && occup_pos.three == empty) {
            check_free_pos();
            draw_X(posThree);
            return 1;
         }

         else if ((occup_pos.five == circle && occup_pos.six == circle) && occup_pos.four == empty) {
            check_free_pos();
            draw_X(posFour);
            return 1;
         }

         else if ((occup_pos.four == circle && occup_pos.six == circle) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }

         else if ((occup_pos.four == circle && occup_pos.five == circle) && occup_pos.six == empty) {
            check_free_pos();
            draw_X(posSix);
            return 1;
         }

         else if ((occup_pos.eight == circle && occup_pos.nine == circle) && occup_pos.seven == empty) {
            check_free_pos();
            draw_X(posSeven);
            return 1;
         }

         else if ((occup_pos.seven == circle && occup_pos.nine == circle) && occup_pos.eight == empty) {
            check_free_pos();
            draw_X(posEight);
            return 1;
         }

         else if ((occup_pos.seven == circle && occup_pos.eight == circle) && occup_pos.nine == empty) {
            check_free_pos();
            draw_X(posNine);
            return 1;
         }

         /* Vertical checks */
         else if ((occup_pos.four == circle && occup_pos.seven == circle) && occup_pos.one == empty) {
            check_free_pos();
            draw_X(posOne);
            return 1;
         }

         else if ((occup_pos.five == circle && occup_pos.eight == circle) && occup_pos.two == empty) {
            check_free_pos();
            draw_X(posTwo);
            return 1;
         }

         else if ((occup_pos.six == circle && occup_pos.nine == circle) && occup_pos.three == empty) {
            check_free_pos();
            draw_X(posThree);
            return 1;
         }

         else if ((occup_pos.one == circle && occup_pos.seven == circle) && occup_pos.four == empty) {
            check_free_pos();
            draw_X(posFour);
            return 1;
         }

         else if ((occup_pos.two == circle && occup_pos.eight == circle) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }
         
         else if ((occup_pos.three == circle && occup_pos.nine == circle) && occup_pos.six == empty) {
            check_free_pos();
            draw_X(posSix);
            return 1;
         }
         
         else if ((occup_pos.one == circle && occup_pos.four == circle) && occup_pos.seven == empty) {
            check_free_pos();
            draw_X(posSeven);
            return 1;
         }

         else if ((occup_pos.two == circle && occup_pos.five == circle) && occup_pos.eight == empty) {
            check_free_pos();
            draw_X(posEight);
            return 1;
         }

         else if ((occup_pos.three == circle && occup_pos.six == circle) && occup_pos.nine == empty) {
            check_free_pos();
            draw_X(posNine);
            return 1;
         }

         /* Diagonal checks */
         else if ((occup_pos.five == circle && occup_pos.nine == circle) && occup_pos.one == empty) {
            check_free_pos();
            draw_X(posOne);
            return 1;
         }

         else if ((occup_pos.five == circle && occup_pos.seven == circle) && occup_pos.three == empty) {
            check_free_pos();
            draw_X(posThree);
            return 1;
         }
         
         else if ((occup_pos.five == circle && occup_pos.three == circle) && occup_pos.seven == empty) {
            check_free_pos();
            draw_X(posSeven);
            return 1;
         }

         else if ((occup_pos.five == circle && occup_pos.one == circle) && occup_pos.nine == empty) {
            check_free_pos();
            draw_X(posNine);
            return 1;
         }

         else if ((occup_pos.one == circle && occup_pos.nine == circle) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }
         
         else if ((occup_pos.three == circle && occup_pos.seven == circle) && occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }
      }
   }

   return 0;
}

/* Takes the center position (if free). Returns 1 if it plays. Returns 0 if it doesn't play */
int play_center()
{
   if (check_win() != 1)
   {
      if (player_is_circle == true)
      {
         if (occup_pos.five == empty) {
            check_free_pos();
            draw_X(posFive);
            return 1;
         }
      }

      else if (player_is_circle == false)
      {
         if (occup_pos.five == empty) {
            check_free_pos();
            draw_circle(posFive);
            return 1;
         }
      }
   }

   return 0;
}

/* Plays at a random position. Used as a last resort thing (if no patterns are found) */
void play_random()
{
   srand(time(NULL));

   while (true)
   {
      int random_num = rand() % 9 + 1;

      if (player_is_circle == true) {
         if (random_num == 1) {
            if (occup_pos.one == empty) {
               check_free_pos();
               draw_X(posOne);
               return;
            }
         }

         else if (random_num == 2) {
            if (occup_pos.two == empty) {
               check_free_pos();
               draw_X(posTwo);
               return;
            }
         }

         else if (random_num == 3) {
            if (occup_pos.three == empty) {
               check_free_pos();
               draw_X(posThree);
               return;
            }
         }

         else if (random_num == 4) {
            if (occup_pos.four == empty) {
               check_free_pos();
               draw_X(posFour);
               return;
            }
         }

         else if (random_num == 5) {
            if (occup_pos.five == empty) {
               check_free_pos();
               draw_X(posFive);
               return;
            }
         }

         else if (random_num == 6) {
            if (occup_pos.six == empty) {
               check_free_pos();
               draw_X(posSix);
               return;
            }
         }

         else if (random_num == 7) {
            if (occup_pos.seven == empty) {
               check_free_pos();
               draw_X(posSeven);
               return;
            }
         }

         else if (random_num == 8) {
            if (occup_pos.eight == empty) {
               check_free_pos();
               draw_X(posEight);
               return;
            }
         }

         else if (random_num == 9) {
            if (occup_pos.nine == empty) {
               check_free_pos();
               draw_X(posNine);
               return;
            }
         }
      }
   }
}

void enemy_turn()
{
   play_random();
   /*if (attack_patterns() == 0)
   {
      if (defense_patterns() == 0) {
         if (play_center() == 0) {
            play_random();
         }
      }
   }*/
}

#endif
