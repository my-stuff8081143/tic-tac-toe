#ifndef SPRITES_H
#define SPRITES_H

#include <ncurses.h>
#include "./logic.h"
#define VERTICAL_BAR "▎"
#define HORIZONTAL_BAR "▁" 
#define BLOCK "▇"

/* Enum for the 9 positions on the board */
enum position
{
   posOne = 1,
   posTwo = 2,
   posThree = 3,
   posFour = 4,
   posFive = 5,
   posSix = 6,
   posSeven = 7,
   posEight = 8,
   posNine = 9
};

/* Draws the tic tac toe board */
void draw_board()
{
   for (int i = 2; i < LINES -2; i++) {
      move(i, (COLS /2) - (COLS /8));
      printw("%s\n", VERTICAL_BAR);
   }   

   for (int i = 2; i < LINES -2; i++) {
      move(i, (COLS /2) + (COLS /8));
      printw("%s\n", VERTICAL_BAR);
   }

   for (int i = ((COLS /2) - (COLS /4)); i < ((COLS /2) + (COLS /4)); i++) {
      move(((LINES /2) - (LINES /6)), i);

      if (i == (COLS /2) - (COLS /8)) {
         printw(" ");
         move(((LINES /2) - (LINES /6) +1), i);
         printw(" ");

         move(((LINES /2) - (LINES /6)), (COLS /2) - (COLS /8) -1);
         printw("  ");
      }

      else if (i == (COLS /2) + (COLS /8)) {
         printw(" ");
         move(((LINES /2) - (LINES /6) +1), i);
         printw(" ");

         move(((LINES /2) - (LINES /6)), (COLS /2) + (COLS /8) -1);
         printw("  ");
      }

      if (!(i == (COLS /2) - (COLS /8)) && !(i == (COLS /2) + (COLS /8))) {
         printw(HORIZONTAL_BAR);
      }
   }   

   for (int i = ((COLS /2) - (COLS /4)); i < ((COLS /2) + (COLS /4)); i++) {
      move(((LINES /2) + (LINES /6)), i);

      if (i == (COLS /2) - (COLS /8)) {
         printw(" ");
         move(((LINES /2) + (LINES /6) +1), i);
         printw(" ");

         move(((LINES /2) + (LINES /6)), (COLS /2) - (COLS /8) -1);
         printw("  ");
      }

      else if (i == (COLS /2) + (COLS /8)) {
         printw(" ");
         move(((LINES /2) + (LINES /6) +1), i);
         printw(" ");

         move(((LINES /2) + (LINES /6)), (COLS /2) + (COLS /8) -1);
         printw("  ");
      }

      if (!(i == (COLS /2) - (COLS /8)) && !(i == (COLS /2) + (COLS /8))) {
         printw(HORIZONTAL_BAR);
      }
   }
}

/* Moves to a specific position on the board */
void move_to_pos(int pos)
{
   if (pos == posOne) {
      move(LINES /5, COLS /3.3);
   }

   else if (pos == posTwo) {
      move(LINES /5, COLS /2);
   }

   else if (pos == posThree) {
      move(LINES /5, (COLS - (COLS /3.3)));
   }

   else if (pos == posFour) {
      move(LINES /2, COLS /3.3);
   }

   else if (pos == posFive) {
      move(LINES /2, COLS /2);
   }

   else if (pos == posSix) {
      move(LINES /2, (COLS - (COLS /3.3)));
   }

   else if (pos == posSeven) {
      move(((LINES) - (LINES /5)), COLS /3.3);
   }

   else if (pos == posEight) {
      move(((LINES) - (LINES /5)), COLS /2);
   }

   else if (pos == posNine) {
      move(((LINES) - (LINES /5)), (COLS - (COLS /3.3)));
   }
}

/* Draws the circle */
void draw_circle(int pos)
{
   if (pos == posOne) {
      occup_pos.one = circle;
      move_to_pos(posOne);
      printw("O");
   }

   else if (pos == posTwo) {
      occup_pos.two = circle;
      move_to_pos(posTwo);
      printw("O");
   }

   else if (pos == posThree) {
      occup_pos.three = circle;
      move_to_pos(posThree);
      printw("O");
   }

   else if (pos == posFour) {
      occup_pos.four = circle;
      move_to_pos(posFour);
      printw("O");
   }

   else if (pos == posFive) {
      occup_pos.five = circle;
      move_to_pos(posFive);
      printw("O");
   }

   else if (pos == posSix) {
      occup_pos.six = circle;
      move_to_pos(posSix);
      printw("O");
   }

   else if (pos == posSeven) {
      occup_pos.seven = circle;
      move_to_pos(posSeven);
      printw("O");
   }

   else if (pos == posEight) {
      occup_pos.eight = circle;
      move_to_pos(posEight);
      printw("O");
   }

   else if (pos == posNine) {
      occup_pos.nine = circle;
      move_to_pos(posNine);
      printw("O");
   }
}

/* Draws the X */
void draw_X(int pos)
{
   if (pos == posOne) {
      occup_pos.one = X;
      move_to_pos(posOne);
      printw("x");
   }

   else if (pos == posTwo) {
      occup_pos.two = X;
      move_to_pos(posTwo);
      printw("x");
   }

   else if (pos == posThree) {
      occup_pos.three = X;
      move_to_pos(posThree);
      printw("x");
   }

   else if (pos == posFour) {
      occup_pos.four = X;
      move_to_pos(posFour);
      printw("x");
   }

   else if (pos == posFive) {
      occup_pos.five = X;
      move_to_pos(posFive);
      printw("x");
   }

   else if (pos == posSix) {
      occup_pos.six = X;
      move_to_pos(posSix);
      printw("x");
   }

   else if (pos == posSeven) {
      occup_pos.seven = X;
      move_to_pos(posSeven);
      printw("x");
   }

   else if (pos == posEight) {
      occup_pos.eight = X;
      move_to_pos(posEight);
      printw("x");
   }

   else if (pos == posNine) {
      occup_pos.nine = X;
      move_to_pos(posNine);
      printw("x");
   }
}

/* Asks the player if he wants to play as circle or X */
/*     true = circle               false = X          */
bool choose_char_scr()
{
   char answer = 0;

   while (answer == 0)
   {
      clear();
      printw("Do you want to play as circle? (Y/N)");
      answer = getch();

      if (answer != 'y' && answer != 'Y' && answer != 'n' && answer != 'N') {
         answer = 0;
      }

      else if (answer == 'y' || answer == 'Y') {
         return true;
      }

      else if (answer == 'n' || answer == 'N') {
         return false;
      }
   }
}

/* Checks if there is an empty position for the board cursor */
int check_occup_board_pos(char input)
{
   if (input == 'd')
   {
      if (current_pos == 1) {
         if (occup_pos.two == empty) {
            return 1;
         }

         else if (occup_pos.three == empty) {
            return 2;
         }

         else if (occup_pos.four == empty) {
            return 3;
         }

         else if (occup_pos.five == empty) {
            return 4;
         }

         else if (occup_pos.six == empty) {
            return 5;
         }

         else if (occup_pos.seven == empty) {
            return 6;
         }

         else if (occup_pos.eight == empty) {
            return 7;
         }

         else if (occup_pos.nine == empty) {
            return 8;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 2) {
         if (occup_pos.three == empty) {
            return 1;
         }

         else if (occup_pos.four == empty) {
            return 2;
         }

         else if (occup_pos.five == empty) {
            return 3;
         }

         else if (occup_pos.six == empty) {
            return 4;
         }

         else if (occup_pos.seven == empty) {
            return 5;
         }

         else if (occup_pos.eight == empty) {
            return 6;
         }

         else if (occup_pos.nine == empty) {
            return 7;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 3) {
         if (occup_pos.four == empty) {
            return 1;
         }
         
         else if (occup_pos.five == empty) {
            return 2;
         }

         else if (occup_pos.six == empty) {
            return 3;
         }

         else if (occup_pos.seven == empty) {
            return 4;
         }

         else if (occup_pos.eight == empty) {
            return 5;
         }

         else if (occup_pos.nine == empty) {
            return 6;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 4) {
         if (occup_pos.five == empty) {
            return 1;
         }

         else if (occup_pos.six == empty) {
            return 2;
         }

         else if (occup_pos.seven == empty) {
            return 3;
         }

         else if (occup_pos.eight == empty) {
            return 4;
         }

         else if (occup_pos.nine == empty) {
            return 5;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 5) {
         if (occup_pos.six == empty) {
            return 1;
         }

         else if (occup_pos.seven == empty) {
            return 2;
         }

         else if (occup_pos.eight == empty) {
            return 3;
         }

         else if (occup_pos.nine == empty) 
         {
            return 4;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 6) {
         if (occup_pos.seven == empty) {
            return 1;
         }

         else if (occup_pos.eight == empty) {
            return 2;
         }

         else if (occup_pos.nine == empty) {
            return 3;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 7) {
         if (occup_pos.eight == empty) {
            return 1;
         }

         else if (occup_pos.nine == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 8) {
         if (occup_pos.nine == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }
   }

   else if (input == 'a')
   {
      if (current_pos == 2) {
         if (occup_pos.one == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 3) {
         if (occup_pos.two == empty) {
            return 1;
         }
         
         else if (occup_pos.one == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 4) {
         if (occup_pos.three == empty) {
            return 1;
         }

         else if (occup_pos.two == empty) {
            return 2;
         }

         else if (occup_pos.one == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 5) {
         if (occup_pos.four == empty) {
            return 1;
         }

         else if (occup_pos.three == empty) {
            return 2;
         }

         else if (occup_pos.two == empty) {
            return 3;
         }

         else if (occup_pos.one == empty) {
            return 4;
         }

         else {
            return 0;
         }
      }
      
      else if (current_pos == 6) {
         if (occup_pos.five == empty) {
            return 1;
         }

         else if (occup_pos.four == empty) {
            return 2;
         }

         else if (occup_pos.three == empty) {
            return 3;
         }

         else if (occup_pos.two == empty) {
            return 4;
         }

         else if (occup_pos.one == empty) {
            return 5;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 7) {
         if (occup_pos.six == empty) {
            return 1;
         }

         else if (occup_pos.five == empty) {
            return 2;
         }

         else if (occup_pos.four == empty) {
            return 3;
         }

         else if (occup_pos.three == empty) {
            return 4;
         }

         else if (occup_pos.two == empty) {
            return 5;
         }

         else if (occup_pos.one == empty) {
            return 6;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 8) {
         if (occup_pos.seven == empty) {
            return 1;
         }

         else if (occup_pos.six == empty) {
            return 2;
         }

         else if (occup_pos.five == empty) {
            return 3;
         }

         else if (occup_pos.four == empty) {
            return 4;
         }

         else if (occup_pos.three == empty) {
            return 5;
         }

         else if (occup_pos.two == empty) {
            return 6;
         }

         else if (occup_pos.one == empty) {
            return 7;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 9) {
         if (occup_pos.eight == empty) {
            return 1;
         }

         else if (occup_pos.seven == empty) {
            return 2;
         }

         else if (occup_pos.six == empty) {
            return 3;
         }

         else if (occup_pos.five == empty) {
            return 4;
         }

         else if (occup_pos.four == empty) {
            return 5;
         }

         else if (occup_pos.three == empty) {
            return 6;
         }

         else if (occup_pos.two == empty) {
            return 7;
         }

         else if (occup_pos.one == empty) {
            return 8;
         }

         else {
            return 0;
         }
      }
   }

   else if (input == 'w') {
      if (current_pos == 4) {
         if (occup_pos.one == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 5) {
         if (occup_pos.two == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 6) {
         if (occup_pos.three == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 7) {
         if (occup_pos.four == empty) {
            return 1;
         }

         else if (occup_pos.one == empty) {
            return 2;
         }
         
         else {
            return 0;
         }
      }

      else if (current_pos == 8) {
         if (occup_pos.five == empty) {
            return 1;
         }

         else if (occup_pos.two == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 9) {
         if (occup_pos.six == empty) {
            return 1;
         }

         else if (occup_pos.three == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }
   }

   else if (input == 's') {
      if (current_pos == 1) {
         if (occup_pos.four == empty) {
            return 1;
         }

         else if (occup_pos.seven == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 2) {
         if (occup_pos.five == empty) {
            return 1;
         }

         else if (occup_pos.eight == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 3) {
         if (occup_pos.six == empty) {
            return 1;
         }

         else if (occup_pos.nine == empty) {
            return 2;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 4) {
         if (occup_pos.seven == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 5) {
         if (occup_pos.eight == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }

      else if (current_pos == 6) {
         if (occup_pos.nine == empty) {
            return 1;
         }

         else {
            return 0;
         }
      }
   }
}

/* The cursor of the board. With it you can select which position to place your character */
void board_cursor(char input)
{
   move_to_pos(current_pos);
   printw(BLOCK);

   if (input == 'd' && current_pos != posNine) {
      move_to_pos(current_pos);
      printw(" ");

      current_pos += check_occup_board_pos('d');

      move_to_pos(current_pos);
      printw(BLOCK);
   }
   
   else if (input == 'a' && current_pos != posOne) {
      move_to_pos(current_pos);
      printw(" ");

      current_pos -= check_occup_board_pos('a');

      move_to_pos(current_pos);
      printw(BLOCK);
   }

   else if (input == 's' && current_pos != posSeven && current_pos != posEight && current_pos != posNine) {
      move_to_pos(current_pos);
      printw(" ");

      current_pos += 3 * check_occup_board_pos('s');

      move_to_pos(current_pos);
      printw(BLOCK);
   }

   else if (input == 'w' && current_pos != posOne && current_pos != posTwo && current_pos != posThree) {
      move_to_pos(current_pos);
      printw(" ");

      current_pos -= 3 * check_occup_board_pos('w');

      move_to_pos(current_pos);
      printw(BLOCK);
   }
}

/* Places either a circle or X down. Returns 1 if char is placed. Returns 0 if no chars are placed */
int place_char(char input)
{
   if (input == '\n')
   {
      if (player_is_circle == false) {
         draw_X(current_pos);
         return 1;
      }

      else if (player_is_circle == true) {
         draw_circle(current_pos);
         return 1;
      }
   }

   return 0;
}

void check_free_pos()
{
   if (check_occup_board_pos('d') != 0 && current_pos != 9) {      
      move_to_pos(current_pos);
      printw(" ");

      current_pos += check_occup_board_pos('d');
   }

   else if (check_occup_board_pos('a') != 0 && current_pos != 1) {
      move_to_pos(current_pos);
      printw(" ");

      current_pos -= check_occup_board_pos('a');
   }         
}

#endif
