#include <locale.h>
#include "./sprites.h"
#include "./logic.h"
#include "./enemy.h"

int main()
{
   setlocale(LC_ALL, "");
   initscr();
   curs_set(0);
   noecho();

   player_is_circle = choose_char_scr();
   clear();
   init_occup_pos_values();
   draw_board();
   char input = 0;
   current_pos = posOne;
   while (check_win() == 0)
   {
      input = getch(); 
      if (input == 'q') break;

      board_cursor(input);

      if (place_char(input) == 1) {
         if (check_occup_board_pos('d') != 0 && current_pos != 9) {
            current_pos += check_occup_board_pos('d');
         }

         else {
            current_pos -= check_occup_board_pos('a');
         }

         move_to_pos(current_pos);
         printw(BLOCK);
      
         enemy_turn();
      }
   }

   endwin();
   return 0;
}
