#ifndef LOGIC_H
#define LOGIC_H

#include <ncurses.h>

bool player_is_circle; // This bool == true if the player is playing as circle
int current_pos;

struct occupying_pos
{
   /* 50 = Neither circle nor X occupying the position */
   /* 51 = circle occupying position */
   /* 52 = X occupying position */
   int one;
   int two;
   int three;
   int four;
   int five;
   int six;
   int seven;
   int eight;
   int nine;
} occup_pos;

enum occupying_pos_values
{
   empty = 50,
   circle = 51,
   X = 52
};

/* Initializes the values of the elements in 'occupying_pos' struct */
void init_occup_pos_values()
{
   occup_pos.one = empty;
   occup_pos.two = empty;
   occup_pos.three = empty;
   occup_pos.four = empty;
   occup_pos.five = empty;
   occup_pos.six = empty;
   occup_pos.seven = empty;
   occup_pos.eight = empty;
   occup_pos.nine = empty;
}

void win_screen()
{
   getch();
   clear();
   move(LINES /2, COLS /2);
   printw("WIN!");
   getch();
}

void lose_screen()
{
   getch();
   clear();
   move(LINES /2, COLS /2);
   printw("LOSE!");
   getch();
}

void tie_screen()
{
   getch();
   clear();
   move(LINES /2, COLS /2);
   printw("TIE!");
   getch();
}

/* Checks if the player wins or loses. Returns 1 for win. Returns 2 for loss. Returns 3 for tie. Returns 0 for neither */
int check_win()
{
   /* Horizontal checks */
   if (occup_pos.one == circle && occup_pos.two == circle && occup_pos.three == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.four == circle && occup_pos.five == circle && occup_pos.six == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.seven == circle && occup_pos.eight == circle && occup_pos.nine == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }   

   else if (occup_pos.one == X && occup_pos.two == X && occup_pos.three == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.four == X && occup_pos.five == X && occup_pos.six == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.seven == X && occup_pos.eight == X && occup_pos.nine == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   /* Vertical checks */
   else if (occup_pos.one == circle && occup_pos.four == circle && occup_pos.seven == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.two == circle && occup_pos.five == circle && occup_pos.eight == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.three == circle && occup_pos.six == circle && occup_pos.nine == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }   

   else if (occup_pos.one == X && occup_pos.four == X && occup_pos.seven == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.two == X && occup_pos.five == X && occup_pos.eight == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.three == X && occup_pos.six == X && occup_pos.nine == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   /* Diagonal checks */
   else if (occup_pos.one == circle && occup_pos.five == circle && occup_pos.nine == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.three == circle && occup_pos.five == circle && occup_pos.seven == circle)
   {
      if (player_is_circle == true) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == false) {
         lose_screen();
         return 2;
      }
   }   

   else if (occup_pos.one == X && occup_pos.five == X && occup_pos.nine == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.three == X && occup_pos.five == X && occup_pos.seven == X)
   {
      if (player_is_circle == false) {
         win_screen();
         return 1;
      }

      else if (player_is_circle == true) {
         lose_screen();
         return 2;
      }
   }

   else if (occup_pos.one != empty &&
            occup_pos.two != empty &&
            occup_pos.three != empty &&
            occup_pos.four != empty &&
            occup_pos.five != empty &&
            occup_pos.six != empty &&
            occup_pos.seven != empty &&
            occup_pos.eight != empty &&
            occup_pos.nine != empty)
   {
      tie_screen();
      return 3;
   }

   return 0;
}

#endif
